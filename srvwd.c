/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#ifdef _BSD_SOURCE                  /* _BSD_SOURCE is only defined on */
#include <bsd/string.h>             /* linux hosts, we need to use the */
#endif                              /* BSD string library */

#include <err.h>
#include <getopt.h>
#include <grp.h>
#include <limits.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>

#include "srvwd.h"
#include "response.h"
#include "request.h"
#include "server.h"


extern char     *__progname;

static void      usage(void);
static void      version(void);

extern volatile sig_atomic_t shutdown_server;
extern volatile sig_atomic_t print_downloads;


/*
 * srvwd is a web server that serves the current working directory.
 *
 * the main function is used to call init, serve, and parse command line
 * options.
 */
int
main(int argc, char **argv)
{
    char path[SRVWD_ROOT_LEN];
    struct passwd *pw;
    struct group *grp;
    char *group = NULL;
    char *user = NULL;
    char *cwd = NULL;
    uint16_t port = SRVWD_DEFAULT_PORT;
    int sockfd = 0;
    int free_user = 0;      /* have to free the user var */
    int free_group = 0;
    int c;
    uid_t uid;
    gid_t gid;
    unsigned short listeners = 1;

    uid = getuid();
    gid = getgid();
    server_chroot = 0;      /* by default, don't chroot */
    memset(path, 0, SRVWD_ROOT_LEN);
    
    while (-1 != (c = getopt(argc, argv, "g:hl:p:ru:v")))
        switch (c) {
            case 'g':
                group = optarg;
                break;
            case 'h':
                usage();
                break;
            case 'l':
                listeners = (unsigned short)atoi(optarg);
                break;
            case 'p':
                port = (uint16_t)atoi(optarg);
                break;
            case 'r':
                server_chroot = 1;
                break;
            case 'u':
                user = optarg;
                break;
            case 'v':
                version();
                exit(EX_OK);
            default:
                usage();
                /* NOTREACHED */
        }

    argc -= optind;
    argv += optind;

    if (NULL == user) {
        if (0 == uid) {
            if (NULL == (user = strdup(SRVWD_DEFAULT_USER))) 
                err(EX_OSERR, "strdup failed!");
        } else {
            pw = getpwuid(uid);
            if (NULL == (user = strdup(pw->pw_name)))
                err(EX_OSERR, "strdup failed!");
        }
        free_user = 1;
    }

    if (NULL == group) {
        if (0 == gid) {
            if (NULL == (group = strdup(SRVWD_DEFAULT_GROUP)))
                err(EX_OSERR, "strdup failed!");
        } else {
            grp = getgrgid(gid);
            if (NULL == (group = strdup(grp->gr_name)))
                err(EX_OSERR, "strdup failed!");
        }
        free_group = 1;
    }

    if (argc > 0)
        strlcpy(path, argv[0], SRVWD_ROOT_LEN);
    else {
        cwd = getcwd(cwd, SRVWD_ROOT_LEN - 1);
        strlcpy(path, cwd, SRVWD_ROOT_LEN);
    }

    if (NULL == path)
        errx(EX_CONFIG, "path cannot be NULL");

    /* bind to socket before setting privileges */
    sockfd = init_socket(port);
    if (-1 == sockfd)
        err(EX_UNAVAILABLE, "failed to set up socket");

    if (EXIT_FAILURE == init_srvwd(path, user, group))
        errx(EX_CONFIG, "initialisation failed.");

    if (free_user) {
        free(user);
        user = NULL;
    }

    if (free_group) {
        free(group);
        group = NULL;
    }
   
    print_downloads = 0;
    shutdown_server = 0;

    if (EXIT_FAILURE == run_server(sockfd, listeners))
        warn("server failed to start");

    close(sockfd);
     
    printf("[+] shutdown complete!\n");
    exit(EXIT_SUCCESS);
}


/*
 * print a simple usage message and exit
 */
static void
usage()
{
    version();
    printf("\nusage: ");
    printf("%s [-g group] [-p port] [-r] [-u user] dir\n", __progname);
    printf("              -g group      group to run as\n");
    printf("              -l listeners  number of listeners to spawn\n");
    printf("              -p port       port to listen on\n");
    printf("              -r            chroot to the cwd\n");
    printf("              -u user       user to run as\n");
    printf("              -v            print version information\n");
    exit(EX_OK);
}


/*
 * print version information
 */
static void
version()
{
    printf("%s version %s\n", __progname, srvwd_VERSION);
}


srvwd - serve working directory
-------------------------------
srvwd is a web server that serves the current working directory.


Dependencies
------------
None.


Compatibility
-------------
srvwd has been tested on the following operating systems:
    * OpenBSD (5.1-snap)
    * OS X (10.8)
    * Linux (Debian 6.0)


Installation
------------
./config.sh
make build install


Usage
-----
srvwd [-g group] [-l listeners] [-p port] [-r] [-u user] [-v] dir

-r will cause srvwd to chroot to the current working directory for security.

Use ^C twice rapidly to halt srvwd. A single ^C will cause srvwd to print
the number of downloads served in the future, although this has not been
implemented yet.


Why require sudo for chroot?
----------------------------
From chroot(2):

     This call is restricted to the super-user.

srvwd uses root privileges to chroot to the target directory, then immediately
drops privileges.


Known bugs / caveats
--------------------
Relative links don't really work. That is, if you have loaded the page
"/foo/bar.html" and there is a link to "quux.html", you would expect to load
"/foo/quux.html". Instead, srvwd currently looks for "/quux.html". This should
be fixed in the near future.


/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/param.h>
#include <sys/types.h>

/* define the default user and group for privilege dropping. */
#define     SRVWD_DEFAULT_USER      (char *)"daemon"
#define     SRVWD_DEFAULT_GROUP     (char *)"bin"

/* define some path lengths */
#define     SRVWD_ROOT_LEN          MAXPATHLEN
#define     SRVWD_FNAM_LEN          2 * MAXPATHLEN

/* define the default port to listen on */
#define     SRVWD_DEFAULT_PORT      8080

/* number of backlogged connections to accept */
#define     SRVWD_BACKLOG           16

/* size of temporary buffer for client connections */
#define     SRVWD_BUFSIZE           255
#define     SRVWD_MAXREQSIZE        4096
#define     SRVWD_CHUNKSIZE         1024

/* various content types */
#define     SRVWD_CT_PLAIN          "text/plain"
#define     SRVWD_EXT_PLAIN         ".txt"
#define     SRVWD_CT_HTML           "text/html"
#define     SRVWD_EXT_HTML          ".html"
#define     SRVWD_CT_CSS            "text/css"
#define     SRVWD_EXT_CSS           ".css"


/*
 * the following two variables are shared across the program to keep a
 * consistent snapshot of where the server is.
 */

/* should we chroot */
int       server_chroot;

/* server root */
char      server_root[SRVWD_ROOT_LEN];


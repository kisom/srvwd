/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#ifdef _BSD_SOURCE                  /* _BSD_SOURCE is only defined on */
#include <bsd/string.h>             /* linux hosts, we need to use the */
#endif                              /* BSD string library */

#include <dirent.h>
#include <err.h>
#include <fcntl.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "content.h"
#include "response.h"
#include "request.h"
#include "srvwd.h"

/*
 * sends the client a 404 page
 */
void
send_client_404(int clientfd, request_type rtype)
{
    char headerstpl[] = "HTTP/1.1 404 Not Found\r\ncontent-type: text/html\r\n"
                        "content-length: %d\r\nconnection: close\r\n"
                        "srvwd-version: %s\r\n\r\n";
    char page404[] = "<!doctype html>\r\n<html>\r\n\t<head>\r\n"
                     "\t\t<meta charset = \"utf-8\">\r\n"
                     "\t\t<title>file not found</title>\r\n"
                     "\t</head>\r\n\r\n\t<body>\r\n"
                     "\t\t<h1>404 - page not found</h1>\r\n"
                     "\t\t<p>The requested page was not found on this "
                     "server.</p>\r\n"
                     "\t</body>\r\n"
                     "</html>\r\n";
    char headers[SRVWD_CHUNKSIZE];
    int headerslen;
    int pagelen;

    memset(headers, 0, SRVWD_CHUNKSIZE);

    pagelen = strlen(page404);
    snprintf(headers, SRVWD_CHUNKSIZE, headerstpl, pagelen, srvwd_VERSION);
    headerslen = strlen(headers);

    write(clientfd, headers, headerslen);

    if (GET == rtype)
        write(clientfd, page404, pagelen);
}


/*
 * send the client a file
 */
void
send_client_file(int clientfd, char *filename, request_type rtype)
{
    char     headerstpl[] = "HTTP/1.1 200 OK\ncontent-type: %s\r\n"
                            "content-length: %d\r\nconnection: close\r\n"
                            "srvwd-version: %s\r\n\r\n";
    char     buf[SRVWD_CHUNKSIZE + 1];
    char    *ct_str;
    struct   stat st;
    FILE    *filep;
    size_t   chunk;
    size_t   progress;
    size_t   read_sz;
    int      headerslen;

    if (-1 == stat(filename, &st)) {
        warn("send client file - stat failed");
        send_client_404(clientfd, rtype);
        return;
    }

    ct_str = get_content_type(filename);
    if (NULL == ct_str) {
        warnx("invalid content type");
        send_client_404(clientfd, rtype);
        return;
    }
    snprintf(buf, SRVWD_CHUNKSIZE, headerstpl, ct_str, st.st_size, 
             srvwd_VERSION);
    headerslen = strlen(buf);

    if (NULL != ct_str) {
        free(ct_str);
        ct_str = NULL;
    }

    filep = fopen(filename, "r");
    if (NULL == filep || -1 == ferror(filep)) {
        warn("error opening %s", filename);
        send_client_404(clientfd, rtype);
    }

    progress = 0;
    write(clientfd, buf, headerslen);

    if (HEAD == rtype)
        return;

    while (progress < (size_t)st.st_size) {
        chunk = st.st_size - progress;
        chunk = chunk > SRVWD_CHUNKSIZE ? SRVWD_CHUNKSIZE : chunk;
        memset(buf, 0, SRVWD_CHUNKSIZE + 1);

        read_sz = fread(buf, sizeof(char), chunk, filep);
        if (write(clientfd, buf, read_sz) != (ssize_t)read_sz) {
            warn("error sending to client");
            break;
        }

        progress += read_sz;
        if (read_sz <= 0) {
            warn("error reading %s (read %u bytes)", filename, 
                 (unsigned)progress);
            break;
        }
    }
    
    if (fclose(filep))
        warn("error closing %s", filename);

    memset(buf, 0, SRVWD_CHUNKSIZE + 1);
}


/*
 * send the client a directory listing
 */
void
send_client_dirlist(int clientfd, char *path, request_type rtype)
{
    char             headertpl[] = "HTTP/1.1 200 OK\r\nconnection: close\r\n"
                                   "content-type: text/html\r\n"
                                   "srvwd-version: %s\r\n\r\n";
    char             headtpl[] = "<!doctype html>\r\n<html>\r\n\t<head>\r\n"
                                 "\t\t<meta charset = \"utf-8\">\r\n"
                                 "\t\t<title>contents of %s</title>\r\n"
                                 "\t</head>\r\n\r\n\t<body>\r\n"
                                 "\t\t<h1>%s</h1>\r\n"
                                 "\t\t<ul>\r\n";
    char             buf[SRVWD_CHUNKSIZE + 1];
    char             footer[] = "\t\t</ul>\r\n\t\t</p>\r\n\t</body>\r\n</html>";
    char             linktpl[] = "\t\t<li><a href=\"%s\">%s</a></li>\r\n";
    char             linkpath[SRVWD_CHUNKSIZE + 1];
    DIR             *dirlist;
    struct dirent   *entry;
    size_t           headerslen;
    size_t           buflen;

    dirlist = opendir(path);
    entry = NULL;

    printf("[+] will try to list %s\n", path);

    if (NULL == dirlist) {
        send_client_404(clientfd, rtype);
        return;
    }

    memset(buf, 0, SRVWD_CHUNKSIZE + 1);
    snprintf(buf, SRVWD_CHUNKSIZE, headertpl, srvwd_VERSION);
    headerslen = strlen(buf);
    write(clientfd, buf, headerslen);
    
    memset(buf, 0, SRVWD_CHUNKSIZE + 1);
    snprintf(buf, SRVWD_CHUNKSIZE, headtpl, path, path);
    buflen = strlen(buf);


    if (HEAD == rtype)
        return;

    write(clientfd, buf, buflen);

    while (NULL != (entry = readdir(dirlist))) {
        memset(buf, 0, SRVWD_CHUNKSIZE + 1);
        memset(linkpath, 0, SRVWD_CHUNKSIZE + 1);

        strncpy(linkpath, entry->d_name, SRVWD_CHUNKSIZE - 1);
        if (DT_DIR == entry->d_type)
            strncat(linkpath, "/", 1);
        snprintf(buf, SRVWD_CHUNKSIZE, linktpl, linkpath,
                 basename(entry->d_name));
        buflen = strlen(buf);
        write(clientfd, buf, buflen);
    };

    write(clientfd, footer, strlen(footer));
    closedir(dirlist);
}


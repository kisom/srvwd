/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#include <sys/types.h>
#ifdef _BSD_SOURCE                  /* _BSD_SOURCE is only defined on */
#include <bsd/string.h>             /* linux hosts, we need to use the */
#endif                              /* BSD string library */

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "content.h"
#include "srvwd.h"


/* 
 * returns an enum representing the content type
 */
CONTENT_TYPE
content_type(char *filename)
{
    CONTENT_TYPE ct = CT_UNKNOWN;
    char *extptr;
    size_t i;

    for (i = strlen(filename); i > 0; --i)
        if ('.' == i)
            break;

    extptr = filename + i;
    if (NULL != strstr(extptr, SRVWD_EXT_PLAIN))
        ct = CT_TEXT;
    else if (NULL != strstr(extptr, SRVWD_EXT_HTML))
        ct = CT_HTML;
    else if (NULL != strstr(extptr, SRVWD_EXT_CSS))
        ct = CT_CSS;
    else if (NULL != strstr(extptr, SRVWD_EXT_PDF))
        ct = CT_PDF;
    else if (NULL != strstr(extptr, SRVWD_EXT_JPEG))
        ct = CT_JPEG;
    else if (NULL != strstr(extptr, SRVWD_EXT_PNG))
        ct = CT_PNG;
    else
        ct = CT_HTML;

    return ct;
}


/*
 * translate a CONTENT_TYPE enum to a string representing the content type
 */
char *
content_type_to_str(CONTENT_TYPE ct)
{
    char *ct_str = NULL;
    int error = 0;
    
    ct_str = calloc(SRVWD_BUFSIZE, sizeof ct_str);
    if (NULL == ct_str)
        return ct_str;

    switch (ct) {
        case CT_TEXT:
            if (strlcpy(ct_str, SRVWD_CT_PLAIN, SRVWD_BUFSIZE) < 1)
                error = 1;
            break;
        case CT_HTML:
            if (strlcpy(ct_str, SRVWD_CT_HTML, SRVWD_BUFSIZE) < 1)
                error = 1;
            break;
        case CT_CSS:
            if (strlcpy(ct_str, SRVWD_CT_CSS, SRVWD_BUFSIZE) < 1)
                error = 1;
            break;
        case CT_JPEG:
            if (strlcpy(ct_str, SRVWD_CT_JPEG, SRVWD_BUFSIZE) < 1)
                error = 1;
            break;
        case CT_PDF:
            if (strlcpy(ct_str, SRVWD_CT_PDF, SRVWD_BUFSIZE) < 1)
                error = 1;
            break;
        case CT_PNG:
            if (strlcpy(ct_str, SRVWD_CT_PNG, SRVWD_BUFSIZE) < 1)
                error = 1;
            break;
        case CT_UNKNOWN:
            error = 1;
            break;
        default:
            /* NOT REACHED */
            error = 1;
            break;
    }

    if (error) {
        free(ct_str);
        ct_str = NULL;
    }

    return ct_str;
}


/*
 * front end to content type code
 */
char *
get_content_type(char *filename)
{
    char *ct_str;
    CONTENT_TYPE ct;

    ct = content_type(filename);
    ct_str = content_type_to_str(ct);

    return ct_str;
}

/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


/* file extensions */
#define     SRVWD_EXT_CSS           ".css"
#define     SRVWD_EXT_HTML          ".html"
#define     SRVWD_EXT_JPEG          ".jpg"
#define     SRVWD_EXT_PDF           ".pdf"
#define     SRVWD_EXT_PNG           ".png"
#define     SRVWD_EXT_PLAIN         ".txt"

/* MIME types */
#define     SRVWD_CT_PDF            "application/pdf"
#define     SRVWD_CT_CSS            "text/css"
#define     SRVWD_CT_HTML           "text/html"
#define     SRVWD_CT_JPEG           "image/jpeg"
#define     SRVWD_CT_PNG            "image/png"
#define     SRVWD_CT_PLAIN          "text/plain"


enum E_CONTENT_TYPE {
    CT_UNKNOWN = -1,
    CT_CSS,
    CT_HTML,
    CT_JPEG,
    CT_PDF,
    CT_PNG,
    CT_TEXT
};

typedef enum E_CONTENT_TYPE CONTENT_TYPE;

CONTENT_TYPE content_type(char *);
char *content_type_to_str(CONTENT_TYPE);
char *get_content_type(char *);

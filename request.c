/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#ifdef _BSD_SOURCE                  /* _BSD_SOURCE is only defined on */
#include <bsd/string.h>             /* linux hosts, we need to use the */
#endif                              /* BSD string library */ 

#include <err.h>
#include <fcntl.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "request.h"
#include "response.h"
#include "srvwd.h"


extern  int       server_chroot;
extern  char      server_root[SRVWD_ROOT_LEN];

static  request_type    get_request_type(char *);


/*
 * determine whether a directory is a valid directory to serve
 */
int
dir_valid(const char *file, int is_dir)
{
    char cwd[SRVWD_FNAM_LEN];
    char base[SRVWD_FNAM_LEN];
    int invalid = -1;         /* ininvalid by default */

    printf("[+] server root: %s\n", server_root);
    memset(cwd, 0, SRVWD_FNAM_LEN);
    memset(base, 0, SRVWD_FNAM_LEN);
    strlcpy(base, file, SRVWD_FNAM_LEN);

    if (NULL == base)
        return invalid;

    if (!is_dir)
        strlcpy(base, dirname(base), SRVWD_FNAM_LEN);

    invalid = chdir(base);

    /* what happens if the cwd is > SRVWD_FNAM_LEN? */
    if (!invalid && NULL != getcwd(cwd, SRVWD_FNAM_LEN)) {
        if (!server_chroot && cwd != strstr(cwd, server_root)) {
            warnx("attempted to request file outside the server root!");
            warnx("attempt to chdir to %s", cwd);
            invalid = -1;
        } else
            invalid = 0;
    } else if (invalid == -1)
        warnx("request made for invalid directory");
    else {
        warnx("could not get cwd");
        invalid = -1;
    }

    return invalid;
}


/*
 * validate the client's request
 */
void
validate_request(int clientfd, char *path, request_type rtype)
{
    char        filename[SRVWD_FNAM_LEN];
    struct stat st;
    int is_dir;

    memset(filename, '\0', SRVWD_FNAM_LEN);
    snprintf(filename, SRVWD_FNAM_LEN - 2, "%s%s", server_root, path);

    printf("[+] will try to serve %s\n", filename);
    if (-1 == stat(filename, &st)) {
        warnx("couldn't stat %s", filename);
        send_client_404(clientfd, rtype);
        return;
    }

    if (st.st_mode & 040000)
        is_dir = 1;
    else
        is_dir = 0;

    if (-1 == dir_valid(filename, is_dir)) {
        send_client_404(clientfd, rtype);
        return;
    }

    if (st.st_mode & 040000) {
        snprintf(filename, SRVWD_FNAM_LEN - 1, "%s%s/index.html", server_root,
                                                                  path);
        if (-1 == stat(filename, &st)) {
            send_client_dirlist(clientfd, dirname(filename), rtype); 
            return;
        }
    }

    send_client_file(clientfd, filename, rtype);
}


/*
 * determines which files to serve and sends the appropriate file off
 * to be sent to the client.
 */
void
request_handler(int clientfd, struct sockaddr_in *client_addr)
{
    char             buf[SRVWD_CHUNKSIZE + 1];
    char             path[SRVWD_ROOT_LEN + 1];
    char            *getptr;
    size_t           lineptr = 0;
    size_t           i, pathidx, startidx, stopidx;
    request_type     req_t;

    printf("[+] connection from %s:%d\n", inet_ntoa(client_addr->sin_addr), 
                                          ntohs(client_addr->sin_port));

    if (-1 == clientfd)
        return;

    memset(buf, '\0', SRVWD_CHUNKSIZE + 1);
    memset(path, '\0', SRVWD_ROOT_LEN + 1);
    getptr = NULL;

    if (read(clientfd, buf, SRVWD_CHUNKSIZE) <= 0) {
        warnx("client read error");
        send_client_404(clientfd, GET);
    }

    while (buf[lineptr] != '\0' && buf[lineptr] != '\r' && 
           buf[lineptr++] != '\n') ;

    buf[lineptr] = '\0';      /* newline, then null byte */
    req_t = get_request_type(buf);

    if (SRVWD_CHUNKSIZE == lineptr) {
        /* request size too large */
        send_client_404(clientfd, req_t);
        return;
    } 

    if (INVALID_METHOD == req_t) {
        warnx("invalid method");
        send_client_404(clientfd, GET);
        return;
    }


    if (NULL == (getptr = strstr(buf, "HTTP/1.1"))) {
        if (NULL == (getptr = strstr(buf, "HTTP/1.0"))) {
            warnx("invalid HTTP request: '%s'", buf);
            send_client_404(clientfd, req_t);
            return;
        }
    }
  
    switch (req_t) {
    case HEAD:
        startidx = 5; /* size of "HEAD" is four characters + SP */
        break;
    case GET:
        startidx = 4;
        break;
    default:
        /* NOTREACHED */
        warnx("invalid state: invalid method in request handler");
        return;
        break;
    }

    stopidx = (getptr - buf) - 1;
    pathidx = 0;
    for (i = startidx; i < stopidx; ++i)
        path[pathidx++] = buf[i];

    for (; pathidx > 0; --pathidx) {
        if (path[pathidx] == ' ' || path[pathidx] == '\t')
            path[pathidx] = '\0';
        else
            break;
    }

    printf("[+] client requested %s\n", path);
    validate_request(clientfd, path, req_t);

}


/*
 * return the request type from a client request
 */
request_type
get_request_type(char *buf)
{
    request_type req;
    size_t i;

    if (buf[0] == '\0') {
        warnx("empty buffer");
        req = INVALID_METHOD;
    } else {
        req = INVALID_METHOD;
        i = 0;
        while (buf[i] != '\x0' && i < SRVWD_MAX_METHODNAM) {
            if ((buf + i) == strstr(buf, "GET")) {
                req = GET;
                break;
            } else if ((buf + i) == strstr(buf, "HEAD")) {
                req = HEAD;
                break;
            }
            ++i;
        }
    }

    return req;
}

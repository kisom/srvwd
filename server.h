/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#ifndef __SRVWD_SERVER_H
#define __SRVWD_SERVER_H

#include <sys/param.h>
#include <sys/types.h>

#include <signal.h>
#include <stdint.h>


volatile sig_atomic_t shutdown_server;
volatile sig_atomic_t print_downloads;

/* used to keep track of the program's uid and gid */
gid_t     _gid;
uid_t     _uid;


int      init_srvwd(char *, char *, char *);
int      get_credentials(char *, char *, uid_t *, gid_t *);
int      init_socket(uint16_t);
int      run_server(int, int);

#endif

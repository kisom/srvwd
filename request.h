/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef __SRVWD_REQUEST_H
#define __SRVWD_REQUEST_H

#include <netinet/in.h>

#include "request.h"

/* 
 * the valid method names are GET, HEAD, POST, PUT, PATCH, and DELETE - 
 * the longest of these is DELETE at six characters.
 */
#define     SRVWD_MAX_METHODNAM         6       

typedef enum request_type {
    INVALID_METHOD = -1,
    GET,
    HEAD,
    POST
} request_type;

int      dir_valid(const char *, int);
void     validate_request(int, char *, request_type);
void     request_handler(int, struct sockaddr_in *);

#endif
